---
title: "track descriptors with context"
author: "Manuel M. Nietert"
date: "`r Sys.Date()`"
output: rmarkdown::html_vignette
fig_caption: yes                    
vignette: >
  %\VignetteIndexEntry{track descriptors}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, echo = FALSE}
knitr::opts_chunk$set(collapse = TRUE, comment = "#>")
```

**track descriptors** is a package intended **for use with** *trajectory data* of an object, a.k.a *tracks* or *traces* or just plain: **the points an object passed through with annotation of the time it happened** :-)

In essence the coordinates define the point in space (*where*) and the time points specify the order of events (*when*), together these mark a track specific *movement pattern*. But since the where has a context we extended the base package to work for instance with blood vessel boundary information, blood flow path.

# load the package
```{r load package, fig.show='hold', echo = TRUE, warning=FALSE, fig.width=7, fig.height=7}

# load the required packages
# the base package
library(trackdescriptors)
# the 3D and context extensions, this also contains the blood vessel example data sets
library(trackdescriptors3D)

```

# plotting tracks to context images

## 2D

```{r, fig.show='hold', echo = FALSE, warning=FALSE, fig.width=5, fig.height=3.5, fig_align='center', fig.cap = "plotting the extracted blood vessel as background."}
library(trackdescriptors)
# example of plotting a processed 2D video snapshot
# a bloodvessel
video2d_proj_as_bg(condition_name = "sample vessel from video",
 slice_matrix = t(example_video_blood_vessel_as_2d_representation_picture),
 video_width = 425.0961,
 video_height = 425.0961)
```


```{r, fig.show='hold', echo = FALSE, warning=FALSE, fig.width=5, fig.height=3.5, fig_align='center', fig.cap = "plotting the tracks upon the extracted blood vessel used as background."}
library(trackdescriptors)
# example of plotting tracks upon a processed 2D video snapshot
# a bloodvessel
# plot the tracks with cluster colors upon 2D-background
plot_tracks_with_cluster_color_from_hclust_w_video2d_proj_as_bg(
 coordinates_list = example_tracks_for_depiction_w_bg[ , c("x.position", "y.position")],
 track_id_list = example_tracks_for_depiction_w_bg[ , "trackid"],
 cluster_ids = example_tracks_for_depiction_w_bg[ , "clusterid"],
 nr_of_clusters = 6,
 condition_name = "example of 2D tracks and background overlay",
 mark_start_end = TRUE,
 video_width = 425.0961,
 video_height = 425.0961,
 slice_matrix = t(example_video_blood_vessel_as_2d_representation_picture))
```


```{r, fig.show='hold', echo = FALSE, warning=FALSE, fig.width=5, fig.height=3.5, fig_align='center', fig.cap = "plotting the tracks upon the extracted blood vessel used as background adding vector field."}
library(trackdescriptors)
# example of plotting tracks upon a processed 2D video snapshot
# a bloodvessel
# plot the tracks with cluster colors upon 2D-background
plot_tracks_with_cluster_color_from_hclust_w_video2d_proj_as_bg(
 coordinates_list = example_tracks_for_depiction_w_bg[ , c("x.position", "y.position")],
 track_id_list = example_tracks_for_depiction_w_bg[ , "trackid"],
 cluster_ids = example_tracks_for_depiction_w_bg[ , "clusterid"],
 nr_of_clusters = 6,
 condition_name = "example of 2D tracks and background overlay",
 mark_start_end = TRUE,
 video_width = 425.0961,
 video_height = 425.0961,
 slice_matrix = t(example_video_blood_vessel_as_2d_representation_picture))

add_arrows_with_mean_start_end_displacement_per_zone(trajectory_table = example_tracks_for_depiction_w_bg, nr_of_x_zones = 15, nr_of_y_zones = 15, lims_to_use = c(0, 400, 0, 400), arrow_color = "red", arrow_lwd = 4)

```


## 3D

```{r, out.width = 400, fig.retina = NULL, echo = FALSE, warning=FALSE, fig.width=5, fig.height=3.5, fig_align='center', fig.cap = "blood vessel 2D depiction loaded into knitr from external file."}
knitr::include_graphics("/home/mnietert/Dokumente/analysen/Michael Haberl Cell Imaging/vessel_system_rgl_3D_top_view.png")
```