#' example of a pixel map of a blood vessel to be used as background
#'
#' an exemplary pixel map which can be used for background of tracks
#' it shows a blood vessel branch from a video
#' It can be seen with \code{image(example_video_blood_vessel_as_2d_representation_picture)}
#' just shoowing the 512x512 pixels with their intensities, or
#' using the video2d_proj_as_bg function
#' \code{video2d_proj_as_bg(condition_name = "sample vessel from video", slice_matrix = example_video_blood_vessel_as_2d_representation_picture, video_width = 425.0961, video_height = 425.0961)}
#'
#' @format A data frame with 512 rows and 512 variables:
#'
#' @source \url{http://ToDolink2HaberlPaper/}
"example_video_blood_vessel_as_2d_representation_picture"

#' example of a 3D pixel map of a blood vessel to be used as background
#'
#' an exemplary pixel map which can be used for background of tracks
#' it contains a blood vessel branch from a video
#' Slices can be seen with
#' \code{image(example_video_blood_vessel_as_2d_representation_picture)}
#' just shoowing the 512x512 pixels with their intensities, or
#' using the video2d_proj_as_bg function
#' \code{video2d_proj_as_bg(condition_name = "sample vessel from video", slice_matrix = example_video_blood_vessel_as_2d_representation_picture, video_width = 425.0961, video_height = 425.0961)}
#'
#' @format A matrix with 512 rows, 512 columns and 23 slices as variables:
#'
#' @source \url{http://ToDolink2HaberlPaper/}
"example_video_snapshot_3d_array"

#' a set of tracks which belongs to an example video
#'
#' these tracks with some clusterids can be used to overlay the background depiction of
#' an example video with the tracks; it contains x y z coordinates so that it can be used
#' with either the 2D or 3D depiction
#'
#' @format A data frame with 634 rows and 7 variables:
#' \describe{
#'   \item{trackid}{the track IDs}
#'   \item{x.position}{the x position}
#'   \item{y.position}{the y position}
#'   \item{z.position}{the z position}
#'   \item{unit}{the units of the coordinates}
#'   \item{time}{the time index, can be used for sorting}
#'   \item{clusterid}{the cluster IDs from cutting an hclust}
#'   \item{treatment}{the name of the treatment}
#' }
"example_tracks_for_depiction_w_bg" 
