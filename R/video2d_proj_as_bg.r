#' plot tracks with cluster color from hclust depiction
#'
#' @param condition_name the title of the plot, e.g stating the treatment condition
#' @param slice_matrix the matrix containing the video intensities as 2D representation
#' @param video_width the width of the video in \eqn{\mu}m
#'        (the same as the coordinates from the tracks)
#' @param video_height the width of the video in \eqn{\mu}m
#'        (the same as the coordinates from the tracks)
#'
#' @return a plot with the tracks in track space colored by cluster_id
#' @export
#'
#' @examples
#' video2d_proj_as_bg(condition_name = "sample vessel from video",
#'  slice_matrix = example_video_blood_vessel_as_2d_representation_picture,
#'  video_width = 425.0961,
#'  video_height = 425.0961)

video2d_proj_as_bg <-
  function (condition_name = "", slice_matrix, video_width = 425.0961, video_height =  425.0961) {
    # the video specifies the screen size not the tracks anymore
    #plotting_range <- c(0, video_width)
    # plotting the background derived from video
    video_resolution <- dim(slice_matrix)[1]
    #video_resolution[1] <- 512
    video_resolution[2] <- dim(slice_matrix)[2]

    single_width_step <- video_width / video_resolution[1]
    single_height_step <- video_height / video_resolution[2]

    max_intensity <- max(slice_matrix, na.rm = T)

    plot(
      0, type = "n", xlim = c(0, video_width), ylim = c(0, video_height), xlab = expression(paste(mu,"m position", sep = "")),  ylab = expression(paste(mu,"m position", sep = "")), main = condition_name
    )

    for (rowindex in 1:video_resolution[1]) {
      for (colindex in 1:video_resolution[2]) {
        if (!is.na(slice_matrix[rowindex, colindex])) {
          points(
            x = (rowindex - 1) * single_width_step,
            y = (colindex - 1) * single_height_step,
            col = adjustcolor("black", slice_matrix[rowindex, colindex] /
                                max_intensity), pch = 20
          )
        }
      }
    }
  }
