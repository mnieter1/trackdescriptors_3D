path_to_exported_channel_pixel_maps <-
  "/home/mnietert/Dokumente/analysen/Michael Haberl Cell Imaging/video_pixelmaps/Pixelmaps_von_160531_SCd2TmbpLA_OX42_M01/channel3"
video_resolution <- 512
#video_resolution[1] <- 512
video_resolution[2] <- 512
video_resolution[3] <- 27


#' import a pixelmatrix stack as 3d_array
#'
#' export pixelmaps with python script in imagej:
#' from ij import IJ
#' from ij.plugin import Duplicator
#'
#' imp = IJ.getImage()
#' h=imp.getHeight()
#' w=imp.getWidth()
#' currentChannel = 3
#'
#' for currentSlice in range(1, imp.getNSlices() + 1):
#'  for currentFrame in range(1, imp.getNFrames() + 1):
#'    slicesImp = Duplicator().run(imp, currentChannel, currentChannel, currentSlice,
#'    currentSlice, currentFrame, currentFrame);
#'    IJ.saveAs(slicesImp, "Text Image", "/path_to_pixmaps/channel3/matrix_" + str(currentSlice) + "_" + str(currentFrame) + ".txt");
#'    slicesImp.close();
#'
#' @param file_folder_path the path to the pixelmaps exports textfiles
#' @param video_resolution a vector specifying the resolution of the video in x y z pixels
#'
#' @return a 3d array of the slices summed for all timepoints
#' @export
#'
#' @examples
#' path_to_exported_channel_pixel_maps <- "/home/mnietert/Dokumente/analysen/Michael Haberl Cell Imaging/video_pixelmaps/Pixelmaps_von_160531_SCd2TmbpLA_OX42_M01/channel3"
#' video_resolution <- 512
#' video_resolution[2] <- 512
#' video_resolution[3] <- 27
#' import_pixelmatrix_stack_as_3d_array(file_folder_path = path_to_exported_channel_pixel_maps, video_resolution = video_resolution)
import_pixelmatrix_stack_as_3d_array <- function(file_folder_path, video_resolution, debug = F){


  path_to_exported_channel_pixel_maps <- file_folder_path

  # set directory
  start_directory <- getwd()
  setwd(path_to_exported_channel_pixel_maps)

  defined_nr_of_slices_in_video <- video_resolution[3]
  # extract information from first slice export
  #  video_resolution <- dim(read.table(file = paste(path_to_exported_channel_pixel_maps, list_of_files[1], sep = "")))


  # Initialize a matrix Mat with zero entries
  video_snapshot_3d_array <-
    array(0, dim = c(video_resolution[1],  video_resolution[2], video_resolution[3]))

  for (slice_index in 1:defined_nr_of_slices_in_video) {
    #find all time points for slice index
    list_of_files <-
      list.files(pattern = paste("matrix_",  as.character(slice_index), "_", sep = ""))



    for (i in 1:length(list_of_files)) {
      if(debug == TRUE){
        print(paste("current file is", list_of_files[i]))
      }
      # load a file from list
      matrix_from_file = read.table(file = list_of_files[i])
      if(debug == TRUE){
        print(paste(
          "video buffer has following dimensions", dim(video_snapshot_3d_array)
        ))
      print(dim(array(as.matrix(video_snapshot_3d_array[ , , slice_index], dim(c(video_resolution[1], video_resolution[2]))) + matrix_from_file, dim = c(video_resolution[1],  video_resolution[2]))))
      }

      video_snapshot_3d_array[ , ,  slice_index] <-
        as.numeric(array(as.matrix(video_snapshot_3d_array[ , , slice_index], dim(c(video_resolution[1], video_resolution[2]))) + as.matrix(matrix_from_file), dim = c(video_resolution[1],  video_resolution[2])))
    }
  }

  # return to starting folder
  setwd(start_directory)

  return(video_snapshot_3d_array)

}
