#' attach min distance to vessel boundary to a coordinate list
#'
#' @param trajectory_table a data.frame with track_ids in column "trackid"
#' @param vessel_borders a coordinate list with reference positions; e.g. vessel borders
#' @param list_of_position_column_names a list containing the names of the columns to use for dist calculation;
#'  c("x.position", "y.position", "z.position")
#'  containing the coordinate information
#' @return a table with the coordinates and track ids with next local x y displacements attached
#' @export
#'
#' @examples
#' # using the example vessel data set
#' trajectory_table <- example_tracks_for_depiction_w_bg
#' # extract the vessel surface
#' video_width <- 425.0961 # (512 pixels)
#' video_height <-  425.0961 # (512 pixels)
#' video_depth <- 150.0893 # (23 pixels)
#' cutoff <- 500
#' # now create the table
#' vessel_borders <- extract_slice_vessel_contour_coordinates_w_give_intensity_cutoff(
#'  a_3D_slice_stack = example_video_snapshot_3d_array,
#'  cutoff = cutoff,
#'  video_width = video_width,
#'  video_height = video_height,
#'  video_depth = video_depth)
#' coordinate_column_names <- c("x.position", "y.position", "z.position")
#' example_tracks_for_depiction_w_bg_w_vesselwalldist <- attach_min_dist_to_vessel_boundary_to_trajectories(trajectory_table,
#'  vessel_borders,
#'  list_of_position_column_names = coordinate_column_names)
attach_min_dist_to_vessel_boundary_to_trajectories <- function (trajectory_table, vessel_borders, list_of_position_column_names){

  # initialize collector
  min_dist_to_ref <- NA

  # for each coordinate; track membership is irrelevant for this
  for (i in 1:dim(trajectory_table)[1]) {

    current_min_dist <- min_dist_to_reference_coordinate_cloud(
      trajectory_table[i , list_of_position_column_names],
      vessel_borders[ , list_of_position_column_names])

    # init the collector if first entry
    if (i == 1) {
      min_dist_to_ref <- current_min_dist
    }else{
      min_dist_to_ref <-
        rbind(min_dist_to_ref, current_min_dist)
    }

  }

  # join together the table with the vectors
  trajectory_table_w_dist_to_vessel_ref_annotations <- data.frame(trajectory_table,
                                                                  min_dist_to_ref = min_dist_to_ref)
  return (trajectory_table_w_dist_to_vessel_ref_annotations)

}
