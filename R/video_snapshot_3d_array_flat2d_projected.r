#' Z project the 3d array
#'
#' @param a_3D_slice_stack holding slices in Z dimension
#'
#' @return a 2D matrix with the summed values over all Z slices
#' @export
#'
#' @examples
#' flat_picture <- video_snapshot_3d_array_flat2d_projected(a_3D_slice_stack = example_video_snapshot_3d_array)
video_snapshot_3d_array_flat2d_projected <-
  function(a_3D_slice_stack) {
    nr_of_slices <- dim(a_3D_slice_stack)[3]

    # collector is
    video_as_2d_representation_picture <-
      matrix(0, nrow = dim(a_3D_slice_stack)[1], ncol = dim(a_3D_slice_stack)[2])

    for (slice_index in 1:nr_of_slices) {
      video_as_2d_representation_picture <-
        video_as_2d_representation_picture + a_3D_slice_stack[, , slice_index]

    }
    return(video_as_2d_representation_picture)
  }
