#' rotate xy matrix positions
#'
#' @param video_snapshot_3d_array a 3D array e.g 3D video snapshot
#'
#' @return the 3D array rotated by x to y clockwise
#' @export
#'
#' @examples
rotate_3d_array_90degrees_x_y <- function(video_snapshot_3d_array) {

  video_dims  <- dim(video_snapshot_3d_array)
  # Initialize a matrix Mat with zero entries
  rotated_video_snapshot_3d_array <-
    array(0, dim = c(video_dims[1],  video_dims[2], video_dims[3]))

  for (slice_index in 1:video_dims[3]) {
    # Flip vertically
    #mtx.tmp.v <- apply(mtx.tmp, 2, rev)
    current_slice <- video_snapshot_3d_array[, , slice_index]
    ## To flip horizontaly
    current_slice_rotated <- t(current_slice)[,nrow(current_slice):1]
    # stuff it back
    rotated_video_snapshot_3d_array[ , , slice_index] <- as.vector(current_slice_rotated)
    # video_snapshot_3d_array[ , ,  slice_index] <-
      # as.numeric(array(as.matrix(video_snapshot_3d_array[ , , slice_index], dim(c(video_dims[1], video_dims[2]))) + as.matrix(current_slice_flipped), dim = c(video_dims[1],  video_dims[2])))
  }
  return(rotated_video_snapshot_3d_array)
}
