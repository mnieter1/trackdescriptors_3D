#' just leave the borders in the matrix
#'
#' this function will scan for any pixels with just filled neighbours and remove them,
#' if applied to a matrix, where a cutoff filter was previously applied e.g.
#' \code{example_matrix[which(example_matrix < cutoff)] <- NA}
#' this will just leave the borders specified by the cutoff
#'
#' @param a_pixelmatrix_w_NAs_and_fill_areas a matrix which contains NAs and values;
#'
#' @return the matrix with removed center pixels
#' @export
#' @examples
#' # get a matrix to use; here we first just create a random number matrix
#' example_matrix <- matrix(rnorm(1000), nrow = 100)
#' # then we set all values smaller than a zero to NA
#' example_matrix[which(example_matrix < 0)] <- NA
#' # stuff it into the smoother function
#' contoured_example_matrix <- remove_inner_pixels(example_matrix)
remove_inner_pixels <-
  function(a_pixelmatrix_w_NAs_and_fill_areas) {
    dimension_of_picture <- dim(a_pixelmatrix_w_NAs_and_fill_areas)

    filtered_matrix <- a_pixelmatrix_w_NAs_and_fill_areas

    # define the modder matrix plus minus in all directions
    modder1 <- c(-1, 0, 1)
    modder2 <- c(-1,-1,-1, 0, 0, 0, 1, 1, 1)

    #cbind((modder2 + 10), (10 + modder1))
    #creates all entries around and at x 10 y 10 :-)

    for (column_position in 1:dimension_of_picture[2]) {
      for (row_position in 1:dimension_of_picture[1]) {
        # do not do this if your at the borders of the matrix
        if (row_position != 1 &
            row_position != dimension_of_picture[1] &
            column_position != 1 & column_position != dimension_of_picture[2]) {
          # do not do this if there is a NA at that position
          if (!is.na(a_pixelmatrix_w_NAs_and_fill_areas[row_position, column_position])) {
            current_sub_matrix_positions <-
              cbind((modder2 + row_position), (column_position + modder1))

            # print(current_sub_matrix_positions)

            # if there is a NA than this sum will be greater than 0
            nr_of_nas <-
              sum(is.na(a_pixelmatrix_w_NAs_and_fill_areas[current_sub_matrix_positions]))

            #print(nr_of_nas)

            # if there is no NA it is in some filled area so shooo go away ;-)
            if (nr_of_nas == 0) {
              filtered_matrix[row_position, column_position] <- NA
            }
          }

        }
      }
    }

    return(filtered_matrix)
  }
