#' flip inverting y positions
#'
#' @param video_snapshot_3d_array a 3D array e.g 3D video snapshot
#'
#' @return the 3D array inverted by y axis
#' @export
#'
#' @examples
flip_3d_array_invert_y <- function(video_snapshot_3d_array) {

  video_dims  <- dim(video_snapshot_3d_array)
  # Initialize a matrix Mat with zero entries
  flipped_video_snapshot_3d_array <-
    array(0, dim = c(video_dims[1],  video_dims[2], video_dims[3]))

  for (slice_index in 1:video_dims[3]) {
    # Flip vertically
    #mtx.tmp.v <- apply(mtx.tmp, 2, rev)
    current_slice <- video_snapshot_3d_array[, , slice_index]
    ## To flip horizontaly
    current_slice_flipped <- apply(current_slice, 2, rev)
    # stuff it back
    flipped_video_snapshot_3d_array[ , , slice_index] <- as.vector(current_slice_flipped)
    # video_snapshot_3d_array[ , ,  slice_index] <-
      # as.numeric(array(as.matrix(video_snapshot_3d_array[ , , slice_index], dim(c(video_dims[1], video_dims[2]))) + as.matrix(current_slice_flipped), dim = c(video_dims[1],  video_dims[2])))
  }
  return(flipped_video_snapshot_3d_array)
}
