#' binarize the matrix set specified reference where there is data
#'
#' @param a_pixelmatrix_w_NAs_and_fill_areas a matrix which contains NAs and values;
#' all values should be set to reference value specified by one_intensity_for_all
#' @param one_intensity_for_all the reference value to be set for all values
#'
#' @return the matrix with filled pixels set to one value
#' @export
#'
#' @examples
#' # get a matrix to use; here we first just create a random number matrix
#' example_matrix <- matrix(rnorm(1000), nrow = 100)
#' # then we set all values smaller than a zero to NA
#' example_matrix[which(example_matrix < 0)] <- NA
#' # stuff it into the binarizer function
#' binarized_example_matrix <- binarize_pixels_na_or_value(example_matrix,
#' one_intensity_for_all = 1)
binarize_pixels_na_or_value <-
  function(a_pixelmatrix_w_NAs_and_fill_areas, one_intensity_for_all = 1) {
    dimension_of_picture <- dim(a_pixelmatrix_w_NAs_and_fill_areas)

    filtered_matrix <- a_pixelmatrix_w_NAs_and_fill_areas

    # creates all entries around and at x 10 y 10 :-)

    for (column_position in 1:dimension_of_picture[2]) {
      for (row_position in 1:dimension_of_picture[1]) {
        # do not do this if your at the borders of the matrix

        # do not do this if there is a NA at that position
        if (!is.na(a_pixelmatrix_w_NAs_and_fill_areas[row_position, column_position])) {
          filtered_matrix[row_position, column_position] <-
            one_intensity_for_all

        }

      }

    }

    return(filtered_matrix)
  }
